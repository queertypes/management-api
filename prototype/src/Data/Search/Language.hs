module Data.Search.Language (
  Field,
  Var,
  Env,
  Type(..),
  Supports(..),
  Op(..),
  Literal(..),
  Expr(..)
) where

import Data.Text (Text, unpack)

type Var = Text
type Field = (Var, Type)
type Env = [Field]
data Type = IntType | StringType | BoolType deriving Eq
data Supports = Equality | Ordering deriving (Eq, Show)

data Literal
  = IntLit Int
  | StringLit Text
  | BoolLit Bool
    deriving Eq

-- normalize exprs to Var-Literal form at parse time
data Expr
  = And Expr Expr
  | BinOpL Op Var Literal
  | BinOpR Op Literal Var
    deriving (Show, Eq)

data Op = Lt | Gt | Eq | Gte | Lte deriving Eq

instance Show Literal where
  show (IntLit n) = "(Int " ++ show n ++ ")"
  show (StringLit t) = "(Str " ++ unpack t ++ ")"
  show (BoolLit b) = "(Bool " ++ show b ++ ")"

instance Show Op where
  show Lt = "<"
  show Gt = ">"
  show Eq = "=="
  show Lte = "<="
  show Gte = ">="

instance Show Type where
  show IntType = "int"
  show BoolType = "bool"
  show StringType = "string"
