{-# LANGUAGE OverloadedStrings #-}
module Data.Search (
  parse,
  eval,
  env
) where

import Data.Text (Text)

import Data.Search.Eval
import Data.Search.Language
import Data.Search.Parser

env :: Env
env = [
--  ("date", DateType)
  ("date", StringType)
  , ("tenant", StringType)
  , ("type", StringType)
  , ("client_id", StringType)
  , ("client_name", StringType)
  , ("user_id", StringType)
  , ("description", StringType)
  , ("user_name", StringType)
  , ("connection", StringType)
  , ("user_agent", StringType)
  , ("impersonator_user_id", StringType)
  , ("impersonator_user_name", StringType)
  , ("connection", StringType)
  , ("connection_type", StringType)
--  , ("ip", IpType)
  , ("ip", StringType)
  , ("count", IntType)
  , ("confidential", BoolType)
  ]
