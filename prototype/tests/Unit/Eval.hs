{-# LANGUAGE OverloadedStrings #-}
module Unit.Eval (
  compileTests
) where

import Data.Either
import Data.Text (Text)
import Test.Tasty
import Test.Tasty.HUnit

import Data.Search
import Data.Search.Eval

right :: Either a b -> b
right (Right x) = x
right _ = error "right: expected Right, actually Left"

evalParse :: Text -> Either Error String
evalParse = eval env . right . parse

typeCheckPassTests :: [TestTree]
typeCheckPassTests =
  [
    testCase "string == string" (
      assert (isRight (evalParse "ip == \"192.168.0.1\"")))
  , testCase "int == int" (
      assert (isRight (evalParse "count == 10")))
  , testCase "bool == cool" (
      assert (isRight (evalParse "confidential == true")))
  ]

typeCheckFailTests :: [TestTree]
typeCheckFailTests =
  let expectTypeError = either isTypeError (const False)
  in [
       testCase "string == int" (
         assert (expectTypeError (evalParse "date == 10")))
     , testCase "string < int" (
         assert (expectTypeError (evalParse "date < 10")))
     , testCase "bool == int (0)" (
         assert (expectTypeError (evalParse "confidential == 0")))
     , testCase "bool == int (1)" (
         assert (expectTypeError (evalParse "1 == confidential")))
     , testCase "bool == string" (
         assert (expectTypeError (evalParse "confidential == \"true\"")))
     ]

unknownFieldTests :: [TestTree]
unknownFieldTests =
  let expectUnknownField = either isUnknownFieldError (const False)
  in [
       testCase "recognize date" (
         assert (isRight (evalParse "date == \"2010-10-10\"")))
     , testCase "unknown dat3" (
         assert (expectUnknownField (evalParse "dat3 == \"2010-10-10\"")))
     , testCase "unknown dat L" (
         assert (expectUnknownField (evalParse "dat == \"2010-10-10\"")))
     , testCase "unknown dat R" (
         assert (expectUnknownField (evalParse "\"2010-10-10\" == dat")))
     ]

operationSupportedPassTests :: [TestTree]
operationSupportedPassTests =
  [
    testCase "int supports Equality" (
      assert (isRight (evalParse "count == 1")))
  , testCase "int supports Ordering" (
      assert (and [ isRight (evalParse "count < 1")
                  , isRight (evalParse "count > 1")
                  ]))
  , testCase "int supports Equality & Ordering" (
      assert (and [ isRight (evalParse "count <= 1")
                  , isRight (evalParse "count >= 1")
                  ]))
  , testCase "string supports Equality" (
      assert (isRight (evalParse "date == \"1\"")))
  , testCase "string supports Ordering" (
      assert (and [ isRight (evalParse "date < \"1\"")
                  , isRight (evalParse "date > \"1\"")
                  ]))
  , testCase "string supports Equality & Ordering" (
      assert (and [ isRight (evalParse "date <= \"1\"")
                  , isRight (evalParse "date >= \"1\"")
                  ]))
  , testCase "bool supports Equality" (
      assert (isRight (evalParse "confidential == true")))
  ]

operationSupportedFailTests :: [TestTree]
operationSupportedFailTests =
  let expectOperationUnsupported = either isOperationNotSupportedError (const False)
  in [
       testCase "bool does not support Ordering" (
         assert (and [ expectOperationUnsupported (evalParse "confidential < true")
                     , expectOperationUnsupported (evalParse "confidential <= true")
                     , expectOperationUnsupported (evalParse "confidential > true")
                     , expectOperationUnsupported (evalParse "confidential >= true")
                     ]))
     ]

compileTests :: TestTree
compileTests = testGroup "compile"
  [
    testGroup "typecheck:success" typeCheckPassTests
  , testGroup "typecheck:fail" typeCheckFailTests
  , testGroup "namecheck" unknownFieldTests
  , testGroup "operationSupported:pass" operationSupportedPassTests
  , testGroup "operationSupported:fail" operationSupportedFailTests
  ]
