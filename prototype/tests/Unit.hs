{-# LANGUAGE OverloadedStrings #-}
module Main where

import Test.Tasty

import Unit.Eval
import Unit.Parser

tests :: TestTree
tests = testGroup "unit"
  [ parserTests
  , compileTests
  ]

main :: IO ()
main = defaultMain tests
