'use strict';

////////////////////////////////////////////////////////////////////////////////
//                             Imports                                        //
////////////////////////////////////////////////////////////////////////////////
const Hapi = require('hapi');
const elasticsearch = require('elasticsearch');
const _ = require('lodash');

////////////////////////////////////////////////////////////////////////////////
//                             Globals                                        //
////////////////////////////////////////////////////////////////////////////////
const server = new Hapi.Server();
server.connection({port: 3000});

const es = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'info',
  apiVersion: '1.7'
});

////////////////////////////////////////////////////////////////////////////////
//                            Validation                                      //
////////////////////////////////////////////////////////////////////////////////
function parseId(val) {
  return new Error('not yet implemented');
}

function parseBool(val) {
  return new Error('not yet implemented');
}

function parseFields(val) {
  return new Error('not yet implemented');
}

function parseSort(val) {
  return new Error('not yet implemented');
}

function parseSearch(val) {
  return new Error('not yet implemented');
}


////////////////////////////////////////////////////////////////////////////////
//                             Handlers                                       //
////////////////////////////////////////////////////////////////////////////////

/*

/api/logs/{_id}

Retrieves the data related to the log entry identified by _id.

*/
server.route({
  method: 'GET',
  path: '/api/logs/{_id}',
  handler: function (req, rep) {
    // var docId = parseId(req.params['_id']);
    var docId = req.params['_id'];
    console.log(docId);
    es.get({
      index: 'logs',
      type: 'log',
      id: docId
    }).then(function(body) {
      console.log(body);
      rep(body);
    }, function(error) {
      console.trace("Document (" + docId + ") not found");
      rep(error);
    })
  }
});

/*
/api/users/{user_id}/logs?page={number}&per_page={items}

Retrieves the log entries related to the user identified by
user_id. Log entries are split into pages of a particular size to
avoid returning all data at once:

* page: The page number. Zero based.
* items: The amount of entries per page.

Response:

{
  'start': 10
  'length': 10,
  'total': 10,
  'limit': 10,
  'logs': [
    {
      'date': '10-10-2010',
      'connection': 'Connection',
      'client_id': '1234',
      'client_name': '1234',
      'ip': '123.12.1.100',
      'user_id': '1234321',
      'user_name': 'cats',
      'description': 'wee yay woo',
      'user_agent': 'lol firefox',
      'type': 's',
      'details': {'data': 'wheaties'}
    }
  ]
}
*/

/* Date Model for events

data Event
  = SuccessLogin  -- s
  | FailedLogin   -- f
  | FailedLoginInvalidEmail -- fu
  | FailedLoginPassword     -- fp
  | FailedByConnector  -- fc
  | FailedByCors -- fco
  | ConnectorOnline -- con
  | ConnectorOffline -- coff
  | SuccessSignup -- ss
  | FailedSignup -- fs
  | SuccessVerificationEmail -- sv
  | FailedVerificationEmail -- fv
  | SuccessChangedPassword -- scp
  | FailedChangePassword -- fcp
  | SuccessChangeEmail -- sce
  | FailedChangeEmail -- fce
  | SuccessVerificationEmailRequest -- svr
  | FailedVerificationEmailRequest -- fvr
  | SuccessChangePasswordRequest -- scpr
  | FailedChangePasswordRequest -- fcpr
  | FailedSendingNotification -- fn
  | UnknownEvent

*/
server.route({
  method: 'GET',
  path: '/api/users/{user_id}/logs',
  handler: function (req, rep) {
    const page = request.query['page'] || 0;
    const per_page = req.query['per_page'] || 20;
    rep('hi');
  }
});

// mutually exclusive: either field retrieval mode or search mode
/*

=== Field Retrieval Mode ===

/api/logs?page=int&per_page=int&sort=(string,-1|1)&exclude_fields=bool

Retrieves data about log entries based on the specified
parameters. Log entries are split into pages of a particular size to
avoid returning all data at once:

* page: The page number. Zero based.
* items: The amount of entries per page.
* field: The field to use for sorting. 1 == ascending and -1 == descending.

* fields: Can be used to either include or exclude the specified
    fields by providing a comma (,) separated list of fields, for
    example at,c,cn,un. If no list is provided all fields are included
    in the response.

* exclude_fields: To exclude the fields exclude_fields=true must be
    used (if no t specified it defaults to false).

Possible values for field are:

* date: The moment when the event occured.
* connection: The connection related to the event.
* client_name: The name of the client related to the event.
* user_name: The user name releated to the event.

=== Search Mode ===

/api/logs?search=string

Retrieves logs that match the specified search criteria. This
parameter can be combined with all the others in the /api/logs
endpoint but is specified separately for clarity.

If no fields are provided a case insensitive 'starts with' search is
performed on all of the following fields:

* client_name
* connection
* user_name

Otherwise, you can specify multiple fields and specify the search
using the %field%:%search%, for example: application:node
user:"John@contoso.com".

Values specified without quotes are matched using a case insensitive
'starts with' search. If quotes are used a case insensitve exact
search is used. If multiple fields are used, the AND operator is used
to join the clauses.

AVAILABLE FIELDS

* application: Maps to the client_name field.
* connection: Maps to the connection field.
* user: Maps to the user_name field.

*/
server.route({
  method: 'GET',
  path: '/api/logs',
  handler: function (req, rep) {
    var page = parseInt(req.query['page']) || 0;
    var per_page = parseInt(req.query['per_page']) || 20;
    var sort = parseSort(req.query['sort']) || {'field': 'date', 'sort': -1};
    var exclude_fields = parseBool(req.query['exclude_fields']) || false;
    var def_fields = ['date', 'connection', 'client_name', 'user_name'];
    var fields = parseFields(req.query['fields']) || (exclude_fields ? [] : def_fields);
    var search = parseSearch(req.query['search']) || '';

    rep('hi');
  }
});

server.start(function() {
  console.log('Server running at:', server.info.uri);
});
